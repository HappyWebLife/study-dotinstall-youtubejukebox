$(function(){

	$('#q').focus();

	//キーワードからYouTubeを検索
	//note :
	//click ではなくsubmitを使うことで、エンターキーで動作させる事ができる。
	$('#search').submit(function(){
		var url = 'http://gdata.youtube.com/feeds/api/videos?' 
				+ [
					'q=' + encodeURIComponent($('#q').val()),
					'alt=json',
					'max-results=10',
					'v=2'
				].join('&');

	//検索結果を#listに追加
		$.get(url,function(data){
			console.log(data);
			$('#list').empty();
			for(var i=0; i<data.feed.entry.length;i++){
				var f = data.feed.entry[i];
				$('#list').append(
					$('<li class="movie">')
					.append($('<img>').attr('src',f['media$group']['media$thumbnail'][0]['url']))
					.data('video-id',f['media$group']['yt$videoid']['$t'])
				);
			}
		},'json');

	});

	//再生
	$(document).on('click','li.movie',function(){
		// player.loadVideoById($(this).data('video-id'));
		$(this).toggleClass('on');
	});

	var currentIndex = 0;

	$('#play').click(function(){
		play();
	});

	$('#pause').click(function(){
		player.pauseVideo();
	});

	$('#next').click(function(){
		if(currentIndex == $('li.movie.on').length - 1){
			currentIndex = 0;
		}else{
			currentIndex++;
		}
		play();
	});

	$('#prev').click(function(){
		if(currentIndex == 0){
			return false;
		}
		currentIndex--;
		play();
	});

	function play(){
		//currentIndexのvideoIDを取得
		var videoId = $('li.movie.on:eq('+currentIndex+')').data('video-id');
		//再生
		player.loadVideoById(videoId);
		//.playingクラスを付与
		$('li.movie').removeClass('playing');
		$('li.movie.on:eq('+currentIndex+')').addClass('playing');
	}



});	

var player;

function onYouTubePlayerAPIReady(){
	player = new YT.Player('player',{
		height: 360,
		width:640,
		events: {
			onStateChange: onPlayerStateChange
		}
	});
}

function onPlayerStateChange(e){
	if(e.data == YT.PlayerState.ENDED){
		$('#next').trigger('click');
	}
}
